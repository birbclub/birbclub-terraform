data "docker_registry_image" "api_image" {
  name = "registry.gitlab.com/birbclub/birbclub-api:latest"
}

resource "docker_image" "api_image" {
  name          = data.docker_registry_image.api_image.name
  pull_triggers = [data.docker_registry_image.api_image.sha256_digest]
}

resource "docker_container" "api" {
  name  = "birbclub-api"
  image = docker_image.api_image.latest

  networks_advanced {
    name = docker_network.network.name
  }

  env = [
    "BIRBCLUB_MINECRAFT_HOST=${docker_container.minecraft.hostname}"
  ]
  
  restart = "always"
}
