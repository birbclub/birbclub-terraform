data "docker_registry_image" "proxy_image" {
  name = "registry.gitlab.com/birbclub/birbclub-proxy:latest"
}

resource "docker_image" "proxy_image" {
  name          = data.docker_registry_image.proxy_image.name
  pull_triggers = [data.docker_registry_image.proxy_image.sha256_digest]
}

resource "docker_container" "proxy" {
  name    = "birbclub-proxy"
  image   = docker_image.proxy_image.latest
  restart = "always"

  networks_advanced {
    name = docker_network.network.name
  }

  ports {
    internal = 80
    external = 80
  }

  ports {
    internal = 443
    external = 443
  }

  volumes {
    container_path = "/var/www/certbot/"
    volume_name    = docker_volume.certbot_www.name
  }

  volumes {
    container_path = "/etc/nginx/ssl/"
    volume_name    = docker_volume.certbot_certs.name
  }
}
