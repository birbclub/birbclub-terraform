data "docker_registry_image" "minecraft_image" {
  name = "registry.gitlab.com/birbclub/birbclub-minecraft"
}

resource "docker_image" "minecraft_image" {
  name          = data.docker_registry_image.minecraft_image.name
  pull_triggers = [data.docker_registry_image.minecraft_image.sha256_digest]
}

resource "docker_container" "minecraft" {
  name    = "birbclub-minecraft"
  image   = docker_image.minecraft_image.latest
  restart = "always"

  networks_advanced {
    name = docker_network.network.name
  }

  ports {
    internal = 25565
    external = 25565
  }

  volumes {
    container_path = "/data"
    host_path      = "/minecraft"
  }

  env = [
    "ENABLE_RCON=TRUE",
    "RCON_PASSWORD=${random_password.rcon_password.result}",
    "VERSION=1.18.1"
  ]
}
