data "docker_registry_image" "content_api_image" {
  name = "registry.gitlab.com/birbclub/birbclub-content-api:latest"
}

resource "docker_image" "content_api_image" {
  name          = data.docker_registry_image.content_api_image.name
  pull_triggers = [data.docker_registry_image.content_api_image.sha256_digest]
}

resource "docker_container" "content_api" {
  name  = "birbclub-content-api"
  image = docker_image.content_api_image.latest

  networks_advanced {
    name = docker_network.network.name
  }

  volumes {
    container_path = "/data"
    host_path      = "/content"
    read_only      = true
  }

  restart = "always"
}
