data "docker_registry_image" "frontend_image" {
  name = "registry.gitlab.com/birbclub/birbclub-frontend:latest"
}

resource "docker_image" "frontend_image" {
  name          = data.docker_registry_image.frontend_image.name
  pull_triggers = [data.docker_registry_image.frontend_image.sha256_digest]
}

resource "docker_container" "frontend" {
  name    = "birbclub-frontend"
  image   = docker_image.frontend_image.latest
  restart = "always"

  networks_advanced {
    name = docker_network.network.name
  }
}
