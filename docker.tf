variable "docker_registry_address" {
  type    = string
  default = "registry.gitlab.com"
}

variable "docker_registry_username" {
  type      = string
  sensitive = true
}

variable "docker_registry_password" {
  type      = string
  sensitive = true
}

provider "docker" {
  host = "unix:///var/run/docker.sock"

  registry_auth {
    address  = var.docker_registry_address
    username = var.docker_registry_username
    password = var.docker_registry_password
  }
}
