resource "docker_image" "certbot_image" {
  name = "certbot/certbot"
}

resource "docker_volume" "certbot_certs" {
  name = "birbclub-certbot-certs"
}

resource "docker_volume" "certbot_www" {
  name = "birbclub-certbot-www"
}

resource "docker_container" "certbot" {
  name  = "birbclub-certbot"
  image = docker_image.certbot_image.latest

  volumes {
    container_path = "/var/www/certbot/"
    volume_name    = docker_volume.certbot_www.name
  }

  volumes {
    container_path = "/etc/letsencrypt/"
    volume_name    = docker_volume.certbot_certs.name
  }
}
