resource "docker_network" "network" {
  name = "birbclub-network"
}

resource "random_password" "rcon_password" {
  length           = 16
  special          = true
  override_special = "_%@"
}